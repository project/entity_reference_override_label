<?php

namespace Drupal\entity_reference_override_label\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\Exception\UndefinedLinkTemplateException;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Render\Markup;

/**
 * Plugin implementation of the 'entity reference override label' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_override_label",
 *   label = @Translation("Override label text"),
 *   description = @Translation("Override the label of referenced entities with tokens and HTML."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceOverrideLabelFormatter extends EntityReferenceLabelFormatter {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'label_override' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['label_override'] = [
      '#title' => $this->t('Override label text'),
      '#type' => 'textfield',
      '#description' => $this->t('You can use tokens and HTML to override the referenced entity label'),
      '#default_value' => $this->getSetting('label_override'),
      '#maxlength' => 200,
    ];

    $target_type = $this->fieldDefinition->getSetting('target_type');
    $token_type = $target_type == 'taxonomy_term' ? 'term' : $target_type;
    $elements['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [$token_type],
      '#global_types' => TRUE,
      '#show_nested' => FALSE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->getSetting('link') ? $this->t('Link to the referenced entity') : $this->t('No link');
    $summary[] = $this->t('Override label text: :label_override', [':label_override' => $this->getSetting('label_override')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $output_as_link = $this->getSetting('link');
    $token = \Drupal::token();

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $label = $this->getSetting('label_override') ?: $entity->label();
      $replace_tokens = [$entity->getEntityTypeId() => $entity];
      $label = Xss::filterAdmin($token->replace($label, $replace_tokens, ['clear' => TRUE]));
      $label = Markup::create($label);

      // If the link is to be displayed and the entity has a uri, display a
      // link.
      if ($output_as_link && !$entity->isNew()) {
        try {
          $uri = $entity->toUrl();
        }
        catch (UndefinedLinkTemplateException $e) {
          // This exception is thrown by \Drupal\Core\Entity\Entity::urlInfo()
          // and it means that the entity type doesn't have a link template nor
          // a valid "uri_callback", so don't bother trying to output a link for
          // the rest of the referenced entities.
          $output_as_link = FALSE;
        }
      }

      if ($output_as_link && isset($uri) && !$entity->isNew()) {
        $elements[$delta] = [
          '#type' => 'link',
          '#title' => $label,
          '#url' => $uri,
          '#options' => $uri->getOptions(),
        ];

        if (!empty($items[$delta]->_attributes)) {
          $elements[$delta]['#options'] += ['attributes' => []];
          $elements[$delta]['#options']['attributes'] += $items[$delta]->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and shouldn't be rendered in the field template.
          unset($items[$delta]->_attributes);
        }
      }
      else {
        $elements[$delta] = ['#markup' => $label];
      }
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

}
